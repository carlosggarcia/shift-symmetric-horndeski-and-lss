#!/usr/bin/env python3
# coding: utf-8

import sacc
import numpy as np
import pyccl as ccl
import os
import matplotlib.pyplot as plt
import wget
import yaml
from scipy.interpolate import interp1d
from prettytable import PrettyTable

##############################################################################
################################ Functions ###################################
##############################################################################
def clean_sacc_file(sinput, config):
    # From MP lkl: https://github.com/carlosggarcia/montepython_public/blob/93aaa6d93d9bced5987340894c168d2debbd9d1e/montepython/likelihoods/cl_cross_corr_v3/__init__.py#L100-L142
    s = sinput.copy()
    # Check used tracers are in the sacc file
    tracers_sacc = [trd for trd in s.tracers]
    for tr in config['tracers'].keys():
        if tr not in tracers_sacc:
            raise ValueError('The tracer {} is not present'.format(tr))
    # TODO: Remove unused tracers
    # used_tracers = config['tracers'].keys()
    # Remove unused Cls
    s.remove_selection(data_type='cl_0b')
    s.remove_selection(data_type='cl_eb')
    s.remove_selection(data_type='cl_be')
    s.remove_selection(data_type='cl_bb')

    # Remove unused tracer combinations
    used_tracer_combinations = []
    for tracers_d in config['tracer_combinations']:
        tr1, tr2 = tracers_d['tracers']
        used_tracer_combinations.append((tr1, tr2))
        # Take into account that both are the same when comparing!
        used_tracer_combinations.append((tr2, tr1))

    used_tracer_combinations_sacc = []
    for tracers in s.get_tracer_combinations():
        if tracers not in used_tracer_combinations:
            s.remove_selection(tracers=tracers)
        used_tracer_combinations_sacc.append(tracers)

    # Cut scales below and above scale cuts for each tracer combination
    for tracers_d in config['tracer_combinations']:
            lmin, lmax = tracers_d['ell_cuts']
            tracers = tuple(tracers_d['tracers'])
            print(tracers, lmin, lmax)
            if tracers not in used_tracer_combinations_sacc:
                # if not present is because they have the opposite ordering
                tracers = tracers[::-1]
            s.remove_selection(ell__lt=lmin, tracers=tracers)
            s.remove_selection(ell__gt=lmax, tracers=tracers)
    print()

    return s

def get_interpolated_cl(cosmo, ls, ccl_tracers, tr1, tr2):
    ls_nodes = np.unique(np.geomspace(2, ls[-1], 30).astype(int)).astype(float)
    cls_nodes = ccl.angular_cl(cosmo,
                               ccl_tracers[tr1],
                               ccl_tracers[tr2],
                               ls_nodes)
    cli = interp1d(np.log(ls_nodes), cls_nodes,
                   fill_value=0, bounds_error=False)
    msk = ls >= 2
    cls = np.zeros(len(ls))
    cls[msk] = cli(np.log(ls[msk]))
    return cls

def get_dtype_for_trs(s, tr1, tr2):
    dt1 = s.get_tracer(tr1).quantity
    dt2 = s.get_tracer(tr2).quantity

    dt_to_suffix = {'galaxy_density': '0', 'cmb_convergence': '0',
                    'galaxy_shear': 'e'}

    dtype = 'cl_'

    dtype += dt_to_suffix[dt1]
    dtype += dt_to_suffix[dt2]

    if dtype == 'cl_e0':
        dtype = 'cl_0e'

    return dtype

def get_ccl_tracers(s, cosmo, bf_pars):
    ccl_trs = {}
    for k, tr in s.tracers.items():
        dtype = tr.quantity
        # Shift dndz
        if dtype != 'cmb_convergence':
            z = tr.z
            nz = tr.nz
            if k + '_dz' in bf_pars:
                z = tr.z - bf_pars[k + '_dz']
                z_sel = z>=0
                z = z[z_sel]
                nz = tr.nz[z_sel]

        if dtype == 'galaxy_density':
            b = bf_pars[k + '_gc_b']
            mag = None
            if 'eBOSS' in k:
                mag = (z, 0.2 * np.ones_like(z))
            ccl_trs[k] = ccl.NumberCountsTracer(cosmo, dndz=(z, nz),
                                        has_rsd=False,
                                        bias=(z, b * np.ones_like(z)), mag_bias=mag)
        elif dtype == 'galaxy_shear':
            A = bf_pars['wl_ia_A']
            eta = bf_pars['wl_ia_eta']
            z0 = 0.62
            IAz = A*((1.+z)/(1.+z0))**eta*0.0139/0.013872474  # pyccl2 -> has already the factor inside. Only needed bz
            ccl_trs[k] = ccl.WeakLensingTracer(cosmo, dndz=(z, nz), ia_bias=(z, IAz))
        elif dtype == 'cmb_convergence':
            ccl_trs[k] = ccl.CMBLensingTracer(cosmo, z_source=1100)
        else:
            raise ValueError(f'Tracer {k} not implemented')
    return ccl_trs

def get_binned_cl(cosmo, s, ccl_tracers, tr1, tr2):
    dtype = get_dtype_for_trs(s, tr1, tr2)
    ind = s.indices(data_type=dtype, tracers=(tr1, tr2))  # not necessary because we have removed all b-correlations
    # Get the bandpower window function.
    # w.values contains the values of ell at which it is sampled
    w = s.get_bandpower_windows(ind)
    # Unbinned power spectrum.
    cl_unbinned = get_interpolated_cl(cosmo, w.values, ccl_tracers, tr1, tr2)
    # Convolved with window functions.
    cl_binned = np.dot(w.weight.T, cl_unbinned)

    return cl_binned

def read_bestfit_MP(fname):
    with open(fname) as f:
        keys = f.readline().replace('#', '').replace(',', '').split()
        vals = [float(i) for i in f.readline().split()]

    bf_pars = dict(zip(keys, vals))
    return bf_pars

def apply_ix_removed(ix_removed, sMP):
    sLin2 = sMP.copy()

    for ix in ix_removed:
        sLin2.remove_indices([ix])

    return sLin2

def scale_cuts_asDES(sMP, mean_lin, mean_nonlin, recalculate=False,
                     outdir='./', prefix=''):
    print('DESY1 like scale cuts')

    fname = '_'.join([prefix, 'ix_removed_inloop_lin_asDES.txt'])
    fname = os.path.join(outdir, fname)
    if os.path.isfile(fname) and not recalculate:
        print('Reading temporal file', fname)
        ix_removed = np.loadtxt(fname, dtype=int)
        sLin2 = apply_ix_removed(ix_removed, sMP)
        return sLin2, ix_removed

    # Starting chi2
    cov = sMP.covariance.covmat
    icov = np.linalg.inv(cov)
    delta = mean_nonlin - mean_lin
    dchi2 = (delta).dot(icov).dot(delta)
    print('DES like starting Delta chi2', dchi2)

    # Sacc instance that will store the final dataset
    sLin2 = sMP.copy()
    # Remove element that contributes the most to dchi2
    ix_removed = []
    while dchi2 > 1:
        for ix_tmp in range(delta.size):
            cov_tmp = np.delete(np.delete(cov, ix_tmp, axis=0), ix_tmp, axis=1)
            icov_tmp = np.linalg.inv(cov_tmp)
            delta_tmp = np.delete(delta, ix_tmp)
            dchi2_tmp = np.abs(delta_tmp.dot(icov_tmp).dot(delta_tmp))
            # Check if removing it decreases dchi2 more than previous elements
            if dchi2_tmp < dchi2:
                ix = ix_tmp
                dchi2 = dchi2_tmp
        # Outside because they must remain the same during the for loop
        cov = np.delete(np.delete(cov, ix, axis=0), ix, axis=1)
        delta = np.delete(delta, ix)
        print(ix, sLin2.data[ix].tracers, dchi2)
        sLin2.remove_indices([ix])
        # Note that if we want to apply ix_removed, we have to do it looping through it,
        # as each index belongs to an array of different dimmension (the
        # previous step is already applied when applying next)
        ix_removed.append(ix)

    ix_removed = np.array(ix_removed)
    np.savetxt(fname, ix_removed.T, fmt='%d')

    return sLin2, np.array(ix_removed)

def scale_cuts_asPlanck(sMP, mean_lin, mean_nonlin, recalculate=False,
                        outdir='./', prefix=''):
    print('Planck15 like scale cuts')

    fname = '_'.join([prefix, 'ix_removed_inloop_lin_asPlanck.txt'])
    fname = os.path.join(outdir, fname)
    if os.path.isfile(fname) and not recalculate:
        print('Reading temporal file', fname)
        ix_removed = np.loadtxt(fname, dtype=int)
        sLin2 = apply_ix_removed(ix_removed, sMP)
        return sLin2, ix_removed

    cov = sMP.covariance.covmat
    icov = np.linalg.inv(cov)
    delta_lin = mean_lin - sMP.mean
    delta_nonlin = mean_nonlin - sMP.mean

    chi2_lin = delta_lin.dot(icov).dot(delta_lin)
    chi2_nonlin = delta_nonlin.dot(icov).dot(delta_nonlin)
    dchi2 = np.abs(chi2_nonlin - chi2_lin)
    print('Planck like starting Delta chi2', dchi2)

    # Sacc instance that will store the final dataset
    sLin2 = sMP.copy()
    # Remove element that contributes the most to dchi2
    ix_removed = []
    while dchi2 > 1:
        # Go through all ellements and check how much it continues to dchi2
        for ix_tmp in range(delta_lin.size):
            cov_tmp = np.delete(np.delete(cov, ix_tmp, axis=0), ix_tmp, axis=1)
            icov_tmp = np.linalg.inv(cov_tmp)

            delta_lin_tmp = np.delete(delta_lin, ix_tmp)
            delta_nonlin_tmp = np.delete(delta_nonlin, ix_tmp)

            chi2_lin_tmp = (delta_lin_tmp).dot(icov_tmp).dot(delta_lin_tmp)
            chi2_nonlin_tmp = (delta_nonlin_tmp).dot(icov_tmp).dot(delta_nonlin_tmp)

            dchi2_tmp = np.abs(chi2_nonlin_tmp - chi2_lin_tmp)
            # Check if removing it decreases dchi2 more than previous elements
            if dchi2_tmp < dchi2:
                ix = ix_tmp
                dchi2 = dchi2_tmp

        # Outside because they must remain the same during the for loop
        cov = np.delete(np.delete(cov, ix, axis=0), ix, axis=1)
        delta_lin = np.delete(delta_lin, ix)
        delta_nonlin = np.delete(delta_nonlin, ix)
        print(ix, sLin2.data[ix].tracers, dchi2)
        sLin2.remove_indices([ix])
        # Note that if we want to apply ix_removed, we have to do it looping through it,
        # as each index belongs to an array of different dimmension (the
        # previous step is already applied when applying next)
        ix_removed.append(ix)

    ix_removed = np.array(ix_removed)
    np.savetxt(fname, ix_removed.T, fmt='%d')

    return sLin2, ix_removed

def print_summary(sLin2, sMP):
    print('Ndp_nonlin =', sMP.mean.size)
    print('Ndp_lin =', sLin2.mean.size)


    # Print summary table
    table = PrettyTable()
    table.field_names = ['Tracers', 'lmin', 'lmin_nonlin', 'lmax',
                         'lmax_nonlin', 'Np', 'Np_nonlin', 'contiguous?']

    print('Cells with non contiguous ells')
    for tr1, tr2 in sLin2.get_tracer_combinations():
        dtype = get_dtype_for_trs(sLin2, tr1, tr2)
        ell_lin, cl_lin = sLin2.get_ell_cl(dtype, tr1, tr2)
        ell_nonlin, cl_data = sMP.get_ell_cl(dtype, tr1, tr2)
        contiguous = np.all(np.diff(np.where(np.in1d(ell_nonlin, ell_lin))) == 1)
        table.add_row(['-'.join([tr1, tr2]), f'{ell_lin[0]:.1f}', f'{ell_nonlin[0]:.1f}',
                       f'{ell_lin[-1]:.1f}', f'{ell_nonlin[-1]:.1f}',
                      ell_lin.size, ell_nonlin.size, contiguous])

        if ~contiguous:
            print((tr1, tr2), np.where(np.in1d(ell_nonlin, ell_lin)))

    print('Summary table')
    print(table)

def main(yaml_file, bestfit_MP_file, asDES=True, asPlanck=True,
         recalculate=False, outdir='./', prefix=''):
    """
    Do all the work. Save remove_indices files.
    """
    ### Read files
    with open(yaml_file) as f:
        config = yaml.safe_load(f)

    bf_pars = read_bestfit_MP(bestfit_MP_file)

    s = sacc.Sacc.load_fits(config['sacc_covG'])
    sMP = clean_sacc_file(s, config)

    #### Get CCL cosmology ####
    cosmo_lin = ccl.Cosmology(Omega_c=bf_pars['Omega_c'], Omega_b=bf_pars['Omega_b'], h=bf_pars['h'],
                              n_s=bf_pars['n_s'], A_s=bf_pars['A_s'],
                              transfer_function='boltzmann_class', matter_power_spectrum='linear')
    cosmo_nonlin = ccl.Cosmology(Omega_c=bf_pars['Omega_c'], Omega_b=bf_pars['Omega_b'], h=bf_pars['h'],
                              n_s=bf_pars['n_s'], A_s=bf_pars['A_s'],
                              transfer_function='boltzmann_class', matter_power_spectrum='halofit')

    #### Get CCL tracers ####
    ccl_trs_lin = get_ccl_tracers(s, cosmo_lin, bf_pars)
    ccl_trs_nonlin = get_ccl_tracers(s, cosmo_nonlin, bf_pars)


    #### Compute the means
    mean_lin = np.array([])
    mean_nonlin = np.array([])
    for tr1, tr2 in sMP.get_tracer_combinations():
        cl_lin = get_binned_cl(cosmo_lin, sMP, ccl_trs_lin, tr1, tr2)
        cl_nonlin = get_binned_cl(cosmo_nonlin, sMP, ccl_trs_nonlin, tr1, tr2)
        for tr in [tr1, tr2]:
            if tr + '_wl_m' in bf_pars:
                cl_lin *= (1 + bf_pars[tr + '_wl_m'])
                cl_nonlin *= (1 + bf_pars[tr + '_wl_m'])
        mean_nonlin = np.concatenate([mean_nonlin, cl_nonlin])
        mean_lin = np.concatenate([mean_lin, cl_lin])
    mean_lin = np.array(mean_lin)
    mean_nonlin = np.array(mean_nonlin)


    # Compute scale cuts
    if asDES:
        sLin2, ix_removed = scale_cuts_asDES(sMP, mean_lin, mean_nonlin,
                                             recalculate, outdir, prefix)
        print_summary(sLin2, sMP)
    if asPlanck:
        sLin2, ix_removed = scale_cuts_asPlanck(sMP, mean_lin, mean_nonlin,
                                                recalculate, outdir, prefix)
        print_summary(sLin2, sMP)

    # Note that if we want to apply ix_removed, we have to do it looping through it,
    # as each index belongs to an array of different dimmension (the
    # previous step is already applied when applying next)


##############################################################################

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Calculate the linear scale cuts for MG")
    parser.add_argument('config', type=str, help='YAML configuration file used in the MCMC')
    parser.add_argument('bestfit', type=str, help='MCMC Bestfit file')
    parser.add_argument('-p', '--prefix', type=str, default='', help='Prefix for the ix_removed files')
    parser.add_argument('-o', '--outdir', type=str, default='./', help='Output directory')
    parser.add_argument('-f', '--force', default=False, action='store_true', help='Recalculate linear cuts')
    parser.add_argument('--only-Planck', default=False, action='store_true', help='Calculate linear cuts only as in Planck15')
    parser.add_argument('--only-DES', default=False, action='store_true', help='Calculate linear cuts only as in DESY1')
    args = parser.parse_args()

    # root = '/mnt/zfsusers/gravityls_3/codes/montepython_cgg_emilio'
    # yaml_file = os.path.join(root, 'cl_cross_corr_params_v3/cl_cross_corr_v3_DES_eBOSS_CMB.yml')
    # bestfit_MP_file = os.path.join(root, 'chains/cl_cross_corr_v3_DES_eBOSS_CMB_mag_correctMag/cl_cross_corr_v3_DES_eBOSS_CMB_mag_correctMag.bestfit')

    if not os.path.isdir(args.outdir):
        os.makedirs(args.makedirs)

    main(args.config, args.bestfit, asDES=not args.only_Planck, asPlanck=not
         args.only_DES, recalculate=args.force, outdir=args.outdir,
         prefix=args.prefix)
