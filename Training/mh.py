#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 12 09:28:45 2021

@author: jessicacowell
"""
import numpy as np
from numpy import random as rand
import matplotlib.pyplot as plt

x  = np.zeros(10000)

def target_dist(x):
    ''' a function that is proportional to the desired probability distribution {\displaystyle P(x)}'''
    return(rand.normal(x))
def jumping_dist(y):
    return(rand.normal(y))

def acceptance_ratio(x, y):
    '''Calculate the acceptance ratio {\displaystyle \alpha =f(x')/f(x_{t})}{\displaystyle \alpha =f(x')/f(x_{t})}, which will be used to decide whether to accept or reject the candidate'''
    return(target_dist(y)/target_dist(x))
    
def random_num():
    '''Generate a uniform random number {\displaystyle u\in [0,1]}{\displaystyle u\in [0,1]}.'''
    return(rand.uniform(0, 1))
    
    
#i = rand.random_integers(0, len(x)-1)
#indices = rand.random_integers(0, len(x)-1)
for i in range(0, 98):
    x_i = x[i]
    x_next = jumping_dist(x_i)
    alpha = acceptance_ratio(x_i, x_next)
    u = random_num()
    if u <= alpha or alpha> 1.0:
        #accept
        x[i+1] = x_next
    elif u > alpha:
        #reject
        x[i+1] = x_i
    else: 
        print('error')
        print(u, alpha)
plt.hist(x)
    
print('success')
print(x)



