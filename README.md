# Shift-symmetric Horndeski and LSS

Shift-symmetric Horndeski models in view of large-scale structure tomographic
data

## Milestones:
 1. Become familiar with theory, data and codes.
 1. Modify the [MontePython likelihood]((https://github.com/xC-ell/xCell-likelihoods/tree/main/MontePython)) to work with hi\_class
 1. Find the scale cuts.
 1. Run the MCMC for the data combinations in [2105.12108](https://arxiv.org/abs/2105.12108)
 1. Write the paper and present your results
 - Extra: Explore the phenomenology; i.e. plot the C_ells for different values of Dina's parameters.

## Theory:
We will focus on the shift-symmetric Horndeski models explored in
[2103.11195](https://arxiv.org/abs/2103.11195) and will replicate Section VI
with large-scale structure tomographic data.

The theoretical priors can be found in https://github.com/dinatraykova/shift_priors

## Data sets:
We will use the data from [2105.12108](https://arxiv.org/abs/2105.12108), i.e.:
 - DESY1 galaxy clustering and weak lensing ([1708.01530](https://arxiv.org/abs/1708.01530))
 - KiDS-1000 weak lensing ([2007.01845](https://arxiv.org/abs/2007.01845))
 - DESI Legacy Survey galaxy clustering ([1804.08657](https://arxiv.org/abs/1804.08657), [2010.00466](https://arxiv.org/abs/2010.00466))
 - eBOSS-QSOs ([2007.08999](https://arxiv.org/abs/2007.08999))
 - Planck18 CMB lensing (convergence field) ([1807.06210](https://arxiv.org/abs/1807.06210))

Available at https://github.com/xC-ell/growth-history

## Scale cuts
We follow DESY1 ([1810.02499](https://arxiv.org/abs/1810.02499)) and Planck15 ([1502.01590](https://arxiv.org/abs/1502.01590)) and choose the scales
that yield `Delta chi2 < 1` for the best fit LCDM model of these data sets.

## Broader background papers
 - `alpha`s: [1404.3713](https://arxiv.org/abs/1404.3713)

# Glamdring
[Glamdring](https://www2.physics.ox.ac.uk/it-services/astrophysics/glamdring-astrophysics-compute-cluster)
is the Astrophysics compute cluster 

## How to launch jobs in Glamdring
Example: 

```addqueue -n 1x16 -m 1 -q cmb -s -c "SD lin scale cuts <1day" /usr/bin/python3 scale-cuts.py```

Here,
 - `-n 1x16`: 1 node and 16 cores per node
 - `-m 1`: 1GB of RAM per core
 - `-q cmb`: Launch job in CMB queue
 - `-s`: Turn off MPI (no multiprocess parallelization); i.e. launch just 1 copy of this job
 - `-c "SD lin scale cuts <1day" `: Comment that you can read when looking what jobs are running/queueing
 - `/usr/bin/python3`: program to launch. `python3` in this case
 - `scale-cuts.py`: argument passed to `python3`, i.e. the program you launch.

## Software that you will need:
 - Dina's modified hi\_class: Linear Pk computation ([hi\_class wiki](https://github.com/miguelzuma/hi_class_public/wiki))
 - Modified MontePython
 ([this](https://github.com/xC-ell/xCell-likelihoods/tree/main/MontePython) or
 [this](https://github.com/carlosggarcia/montepython_public/tree/emilio)):
 Parameter space sampling (MCMC)
 - [sacc](https://github.com/LSSTDESC/sacc): data management
 - [pyccl](https://github.com/LSSTDESC/CCL): C_ell computation
 - [GetDist](https://github.com/cmbant/getdist): Chain analysis and plotting

### How to install python libraries
To install python libraries in Glamdring do 

```pip3 install <package name> --user```

Note the`3`. In Glamdring, the default python version is `2`. You will want to
run all these codes with `python3`, though. Recall to append the `3` to your
python commands.
